"""
Lädt und verwaltet die Datenstrukturen im Hintergrund.

--- OOA von body einfügen ---
"""
import json

_all_data = {}

with open("bodies.json") as file:
    _all_data = json.load(file)

sun = _all_data[
    "properties"
]  # Der oberstgeordnete Körper ist die Sonne, daher hat er keine Beschriftung
bodies = {}
for name, body in _all_data["satellites"].items():
    body["name"] = name
    try:
        for name_satellite, satellite in body["satellites"].items():
            satellite["name"] = name_satellite
            for name_commodity, commodity in satellite["goods"].items():
                commodity["name"] = name_commodity
    except KeyError:  # No satellites
        pass
    try:
        for name_commodity, commodity in body["goods"].items():
            commodity["name"] = name_commodity
    except KeyError:
        pass
    bodies[name] = body

bodies_by_id = {}  # bodies, indexiert nach ids

del _all_data

if __name__ == "__main__":
    print(sun)
    print(bodies)
