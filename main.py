# -*- coding: UTF-8 -*-
"""Hauptmodul des Spiels."""
import math
import time
import tkinter

import data

player = dict()


def comm_constr(name, amount=0, **kwargs):
    kwargs["name"] = name
    kwargs["amount"] = amount
    return kwargs


def trade(player, commodity, amount):
    """
    Handele amount Tonnen von commodity.

    dict player: Spieler, welcher handelt
    dict commodity: Ware, mit welcher gehandelt wird
    int amount: Menge (in t), die gehandelt wird; negativ bedeutet Verkauf
    """
    print("Trade called:")
    print("player", player)
    print("commodity", commodity)
    print("amount", amount)
    total_weight = amount
    total_price = commodity["price"] * amount
    # v Das Gewicht, welches hinzugefügt wird, falls der Handel erfolgreich ist
    for commodity2 in player["inventory"].values():
        total_weight += commodity2["amount"]  # < choose a better fucking name
    del commodity2
    try:
        if (
            player["crew"] * 500 < total_weight
            or total_price > player["bal"]
            or player["inventory"][commodity["name"]]["amount"] + amount < 0
        ):
            print("Couldn't execute trade.")
            return False
        else:
            player["bal"] -= total_price
            player["inventory"][commodity["name"]]["amount"] = (
                player["inventory"][commodity["name"]]["amount"] + amount
            )
    except KeyError:  # _Could_ cause problems when selling. shouldn't
        player["inventory"][commodity["name"]] = comm_constr(
            commodity["name"], amount
        )
        # ^ commodity should always stem from body["goods"]
    print("player after trade:")
    print(player)
    return True


def select(event):
    global player
    global date
    print("select called")
    click_data = event.widget.find_overlapping(
        (event.x - 1), (event.y - 1), (event.x), (event.y)
    )
    print(click_data)
    try:
        body = data.bodies_by_id["main"][click_data[0]]
        player["goal"] = body["name"]
        if player["goal"] == player["location"]:
            try:
                if "satellites" not in data.bodies[player["location"]]:
                    tradeui.init_window(
                        player, data.bodies[player["location"]], window
                    )
                else:
                    date, player, data.bodies[
                        player["location"]
                    ] = orbitsystem.system(
                        orbitsystem.data.bodies[player["location"]],
                        500,
                        date,
                        player,
                        window,
                    )
            except IndexError:  # Where does this happen?
                print("unbound")

    except IndexError:
        pass


def main(window):  # Privat oder nicht? Potenzielle API? Docstring ausbaufähig
    """Hauptfunktion des Moduls."""
    global date
    global player
    player["inventory"] = {"food": comm_constr("food", 10)}
    player["goal"] = "Mars"
    player["ship_id"] = -1  # 0 can be an id, -1 never.
    player["bal"] = 1000
    player["location"] = "Mars"
    player["crew"] = 7
    size = 700
    window.title("SpaceWars")
    canvas = tkinter.Canvas(window, width=size, height=size, bd=0, bg="black")
    canvas.pack()
    orbitsystem.stars(canvas, 600, size)
    canvas.update()
    date = 100  # This way, planets are jumbled

    def shipupdate(bodies, canvas, player, date):
        if player["ship_id"] not in canvas.find_all():
            totalcoords = canvas.coords(bodies[player["location"]]["id"])
            print(totalcoords)
            x = (totalcoords[0] + totalcoords[2]) / 2
            y = (totalcoords[1] + totalcoords[3]) / 2
            print(x, y)
            player["ship_id"] = canvas.create_polygon(
                x + 3, y, x - 3, y, x, y - 3, x, y + 3, fill="red"
            )
        totalcoords = canvas.coords(bodies[player["goal"]]["id"])
        xg = (totalcoords[0] + totalcoords[2]) / 2
        yg = (totalcoords[1] + totalcoords[3]) / 2
        totalcoords = canvas.coords(player["ship_id"])
        x = (totalcoords[0] + totalcoords[2]) / 2
        y = (totalcoords[1] + totalcoords[3]) / 2
        # showme = canvas.create_line(xg,yg,x,y)
        shipspeed = 2
        if bodies[player["goal"]]["id"] not in canvas.find_overlapping(
            x + 1, y + 1, x - 1, y - 1
        ):
            m = (y - yg) / (x - xg)
            x_movement = 1 / math.sqrt(
                abs(shipspeed + (m * (shipspeed - xg) + yg))
            )
            if x < xg:
                move_to_y = (m * ((x + shipspeed + x_movement) - xg)) + yg
                canvas.coords(
                    player["ship_id"],
                    x + 3 + shipspeed,
                    move_to_y,
                    x - 3 + shipspeed,
                    move_to_y,
                    x + shipspeed,
                    move_to_y - 3,
                    x + shipspeed,
                    move_to_y + 3,
                )
            else:
                move_to_y = (m * ((x - shipspeed - x_movement) - xg)) + yg
                canvas.coords(
                    player["ship_id"],
                    x + 3 - shipspeed,
                    move_to_y,
                    x - 3 - shipspeed,
                    move_to_y,
                    x - shipspeed,
                    move_to_y - 3,
                    x - shipspeed,
                    move_to_y + 3,
                )
        else:
            canvas.delete(player["ship_id"])
            player["location"] = player["goal"]
            canvas.itemconfigure(
                location_text, text="location = " + player["goal"]
            )
            try:
                if "satellites" not in data.bodies[player["location"]]:
                    tradeui.init_window(
                        player, data.bodies[player["location"]], window
                    )
                else:
                    date, player, data.bodies[
                        player["location"]
                    ] = orbitsystem.system(
                        orbitsystem.data.bodies[player["location"]],
                        500,
                        date,
                        player,
                        window,
                    )
            except UnboundLocalError:  # Where does this happen?
                print("unbound")

    location_text = canvas.create_text(
        2,
        5,
        text="location = " + player["location"],
        anchor="nw",
        fill="white",
    )
    time_text = canvas.create_text(
        2, 15, text="Date: 0", anchor="nw", fill="white"
    )
    bal_text = canvas.create_text(
        2, 25, text="Bal: " + str(player["bal"]), anchor="nw", fill="white"
    )
    food_text = canvas.create_text(
        2,
        35,
        text="Food: " + str(player["inventory"]["food"]["amount"]),
        anchor="nw",
        fill="white",
    )
    canvas.bind("<ButtonRelease-1>", select)

    # Darstellung der Planeten und Sonne, in Orbitsystem integrieren?
    big_orbit = 0
    orbit_modifier = 0
    for body in data.bodies.values():
        if big_orbit < body["properties"]["orbit_radius"]:
            big_orbit = body["properties"]["orbit_radius"]
    try:
        orbit_modifier = (size / 2 - (size / 2 / 10)) / big_orbit
        sun_size = data.sun["radius"] * orbit_modifier
    except ZeroDivisionError:
        sun_size = size / 2 - (size / 2 / 2)
    canvas.create_oval(
        (size / 2) - sun_size,
        (size / 2) - sun_size,
        (size / 2) + sun_size,
        (size / 2) + sun_size,
        fill=data.sun["colour"],
    )

    for body in data.bodies.values():
        orbitsystem.planet(canvas, body, size, orbit_modifier)
    # print(data.bodies_by_id["main"])

    canvas.update()
    while True:
        time.sleep(0.1)
        canvas.itemconfigure(time_text, text="Date: " + str(date))
        canvas.itemconfigure(bal_text, text="Bal: " + str(player["bal"]))
        canvas.itemconfigure(
            food_text,
            text="Food: " + str(player["inventory"]["food"]["amount"]),
        )
        date += 1
        if date % (1550) // player["crew"] == 0:
            player["inventory"]["food"]["amount"] -= 1
        if date % 7 == 0:
            player["bal"] -= 1 * player["crew"]
        # v Man darf nicht pleite gehen‽
        if player["inventory"]["food"]["amount"] < 0 or player["bal"] < 0:
            for widget in canvas.find_all():
                canvas.delete(widget)
            player["crew"] = 0
            canvas.create_text(
                size / 2, size / 2, text="GAME OVER", fill="white"
            )
            canvas.create_text(
                size / 2,
                size / 2 + 20,
                text="Final score: " + str(player["bal"]),
                fill="white",
            )
            while True:
                canvas.update()

        if player["goal"] != player["location"]:
            shipupdate(data.bodies, canvas, player, date)
        orbitsystem.update_planets(
            data.bodies, date, canvas, size, orbit_modifier
        )


if __name__ == "__main__":  # Wenn Modul einzeln aufgerufen wird
    import orbitsystem
    import tradeui

    quit = False
    orbitsystem.init(data)
    window = tkinter.Tk()
    try:
        main(window)
    except tkinter._tkinter.TclError:  # Fenster geschlossen, das ist kein Fehler.
        quit = True
    finally:
        if not quit:
            window.destroy()
