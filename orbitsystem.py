# -*- coding: UTF-8 -*-
import math
import random
import time
import tkinter
from pprint import pprint

import tradeui

data = None


def init(data_main):
    global data
    data = data_main


def stars(canvas, amount, size):
    for i in range(amount):
        colour = (
            "#"
            + hex(int((((i) / amount) ** 2 * 254) + 1.5))[2:] * 3
            # ^ more dark stars than bright ones
        )
        locationx = random.randint(0, size)
        locationy = random.randint(0, size)
        canvas.create_oval(
            locationx + 2,
            locationy + 2,
            locationx - 1,
            locationy - 1,
            fill=colour,
        )


def planet(canvas, body, size, orbit_modifier, system="main"):
    """
    Kreiere einen Planeten auf canvas. DOCSTRING NICHT MEHR AKTUELL

    Canvas canvas: Leinwand, auf der der Planet dargestellt wird
    str name: Name des Planeten
    float orbit_radius: Radius der Darstellung des Planeten
    float orbit_time: Benötigte Zeit um einmal den Orbit zu durchlaufen
    bool retrograde: ob der Planet im Uhrzeigersinn dreht
    float radius: Radius des Planeten
    str colour: Farbe des Planeten
    int size: Größe des canvas
    """
    pprint(("new planet:", canvas, body, size, orbit_modifier, system))
    orbit_radius = body["properties"]["orbit_radius"] * orbit_modifier

    body["orbit_id"] = canvas.create_oval(
        (size / 2) - orbit_radius,
        (size / 2) - orbit_radius,
        (size / 2) + orbit_radius,
        (size / 2) + orbit_radius,
        activeoutline="green",
        outline="#444444",
    )

    orbit_radius = body["properties"]["orbit_radius"] * orbit_modifier
    body["id"] = canvas.create_oval(
        ((size / 2) + orbit_radius) - 3,
        (size / 2) - 3,
        ((size / 2) + orbit_radius) + 3,
        (size / 2) + 3,  # body["properties"]["radius"]
        fill=body["properties"]["colour"],  # fill=body["properties"]["colour"]
        activeoutline="green",
    )
    body["properties"]["angle_per_tick"] = (2 * math.pi) / body["properties"][
        "orbit_time"
    ]
    if system not in data.bodies_by_id:
        data.bodies_by_id[system] = {}
    pprint(("bodies_by_id system", data.bodies_by_id[system]))
    canvas.create_text(
        size / 2 + orbit_radius,
        len(data.bodies_by_id[system]) * 10,
        text=body["name"],
        anchor="ne",
        fill="white",
    )  # Needs improvement

    data.bodies_by_id[system][body["id"]] = body
    data.bodies_by_id[system][body["orbit_id"]] = body
    pprint(("bodies_by_id system", data.bodies_by_id[system]))

    return canvas.coords(body["id"])


def update_planets(bodies, date, canvas, size, orbit_modifier):
    """
    Aktualisiere die Darstellung der Planeten auf canvas.

    dict all_the_bodies: die daten aller satelliten
    int date: Datum in Erdtagen seit Spielstart?
    Canvas canvas: Leinwand, auf der die Planeten dargestellt werden
    """
    for body in bodies.values():
        try:
            angle = (
                (-1 if body["properties"]["retrograde"] else 1)
                * body["properties"]["angle_per_tick"]
                * date
            )
        except KeyError:
            angle = body["properties"]["angle_per_tick"] * date
        y = (
            math.sin(angle)
            * body["properties"]["orbit_radius"]
            * orbit_modifier
        )
        x = (
            math.cos(angle)
            * body["properties"]["orbit_radius"]
            * orbit_modifier
        )
        canvas.coords(
            body["id"],
            (x - 2) + (size / 2),
            (y - 2) + (size / 2),
            (x + 2) + (size / 2),
            (y + 2) + (size / 2),
        )
        # pprint(x - body["properties"]["radius"])
    canvas.update()


def system(body, size, date, player, main_window=None):
    """
    Generiert ein Körpersystem.

    str name: name des Haupt körpers
    dict all_datal: die gesamte information des systems
    int size: die Größe des fensters
    float dateg: das Datum
    """

    def select(event, system=body["name"], player=player):
        click_data = event.widget.find_overlapping(
            (event.x - 1), (event.y - 1), (event.x), (event.y)
        )
        print("orbitsystem.select called")
        pprint(("click_data", click_data))
        pprint(("system", system))
        pprint(("coords", canvas.coords(click_data)))
        try:  # v this is fucking ugly
            pprint(("bodies_by_id system", data.bodies_by_id[system]))
            name = data.bodies_by_id[system][click_data[0]]["name"]
            try:
                print("bodies in orbitsystem:")
                pprint(data.bodies)
                tradeui.init_window(player, data.bodies[name], window)
            except KeyError:
                tradeui.init_window(
                    player,
                    data.bodies[body["name"]]["satellites"][name],
                    window,
                )
        except IndexError:
            print(
                "index error thrown in orbitsystem.py l.144-152, click on bg"
            )

    window = tkinter.Toplevel(main_window) if main_window else tkinter.Tk()
    window.title(body["name"])
    canvas = tkinter.Canvas(window, width=size, height=size, bd=0, bg="black")
    canvas.pack()
    canvas.bind("<ButtonRelease-1>", select)
    timetext = canvas.create_text(
        2, 15, text="Date: 0", anchor="nw", fill="white"
    )
    baltext = canvas.create_text(
        2, 25, text="Bal: " + str(player["bal"]), anchor="nw", fill="white"
    )
    foodtext = canvas.create_text(
        2,
        35,
        text="Food: " + str(player["inventory"]["food"]["amount"]),
        anchor="nw",
        fill="white",
    )
    try:
        big_orbit = 0
        for i in body["satellites"]:
            print(i)
            if big_orbit < body["satellites"][i]["properties"]["orbit_radius"]:
                big_orbit = body["satellites"][i]["properties"]["orbit_radius"]
        orbit_modifier = (size / 2 - (size / 2 / 10)) / big_orbit
        pprint((big_orbit, size, orbit_modifier))
        planet_size = body["properties"]["radius"] * orbit_modifier
        for satellite in body["satellites"].values():
            planet(canvas, satellite, size, orbit_modifier, body["name"])
    except IndexError:
        print("IndexError in orbitsystem, setting orbit_modifier to 1 l.192")
        orbit_modifier = 1
        planet_size = size / 2 - (size / 2 / 2)
    body["id"] = canvas.create_oval(
        (size / 2) - planet_size,
        (size / 2) - planet_size,
        (size / 2) + planet_size,
        (size / 2) + planet_size,
        fill=body["properties"]["colour"],
    )
    data.bodies_by_id[body["name"]][body["id"]] = body
    pprint(("bodies_by_id", data.bodies_by_id))
    canvas.update()
    # date = date * 144  # DRY
    try:
        while True:
            time.sleep(0.1)
            canvas.itemconfigure(
                timetext, text="Date: {0}".format(round(date, 2))
            )
            canvas.itemconfigure(
                baltext, text="Bal: {0}".format(player["bal"])
            )
            canvas.itemconfigure(
                foodtext,
                text="Food: {0}".format(player["inventory"]["food"]["amount"]),
            )
            date += 1 / 144
            if date % 2 == 0:
                if date % int((1550) / player["crew"]) == 0:
                    player["inventory"]["food"]["amount"] -= (
                        1 * player["inventory"]["crew"]["num"]
                    )
                if date % 7 == 0:
                    player["bal"] -= 1 * player["crew"]
                if player["inventory"]["food"]["amount"] < 0:
                    for widget in canvas.find_all():
                        canvas.delete(widget)
                    player["crew"] = 0
                    canvas.create_text(
                        size / 2, size / 2, text="GAME OVER", fill="white"
                    )
                    canvas.create_text(
                        size / 2,
                        size / 2 + 20,
                        text="Final score: " + str(player["bal"]),
                        fill="white",
                    )
            update_planets(
                body["satellites"], date, canvas, size, orbit_modifier
            )
    except tkinter._tkinter.TclError:
        player["goal"] = body["name"]
        player["location"] = body["name"]
        return int(date), player, body


##    finally:
##        tk.destroy()

if __name__ == "__main__":
    import data
    from main import comm_constr

    system(
        data.bodies["Jupiter"],
        400,
        1000,
        {
            "crew": 7,
            "inventory": {
                "food": comm_constr("food", 10),
                "uranium": comm_constr("uranium", 34),
            },  # handle undefined
            "goal": "Mars",
            "ship_id": 0,
            "bal": 100000,
            "location": "Mars",
        },
    )
