import tkinter

from main import comm_constr, trade


def _trade_update(player, commodity, amount):
    pass


def init_window(player, body, main_window=None):
    """Kreiert ein Handelsfenster für den Handel zwischen player und der Station bei body."""
    window = tkinter.Toplevel(main_window) if main_window else tkinter.Tk()
    window.title(f"{body['name']} – Handel")

    ship_data = {"goods_by_id": []}
    station_data = {"goods_by_id": []}

    # v DRY, well fuck. also the ship's inventory area thingy
    ship_data["frame"] = tkinter.LabelFrame(window, text="Schiff")
    ship_data["frame"].grid(row=0, column=0, padx=5, pady=5, rowspan=2)
    ship_data["listbox"] = tkinter.Listbox(
        ship_data["frame"],
        highlightthickness=0,
        borderwidth=0,
        activestyle="none",
        selectmode="extended",
        height=10,  # um yeah scrolling
    )
    print("player", player)
    for commodity_name, commodity in player["inventory"].items():
        ship_data["listbox"].insert(
            "end", f"{commodity['amount']}t {commodity_name}"
        )
        ship_data["goods_by_id"].append(commodity)
    for id, commodity in enumerate(ship_data["goods_by_id"]):
        commodity["id"] = id
        print("appended id:", commodity)
    print(player["inventory"])
    ship_data["listbox"].pack()

    # v here go the buttons in between
    tkinter.Button(
        window,
        text=">",
        command=lambda: [
            _trade_update(
                body["goods"][ship_data["goods_by_id"][commodity_id]["name"]],
                int(amount_entry.get()) * -1,
                ship_data["listbox"],
                commodity_id,
            )
            for commodity_id in ship_data["listbox"].curselection()
        ],
    ).grid(row=0, column=1, padx=5, pady=5)
    tkinter.Button(
        window,
        text="<",
        command=lambda: [
            _trade_update(
                station_data["goods_by_id"][commodity_id],
                int(amount_entry.get()),
                station_data["listbox"],
                commodity_id,
            )
            for commodity_id in station_data["listbox"].curselection()
        ],
    ).grid(row=1, column=1, padx=5, pady=5)

    # v this be tha station
    station_data["frame"] = tkinter.LabelFrame(window, text="Station")
    station_data["frame"].grid(row=0, column=2, padx=5, pady=5, rowspan=2)
    station_data["listbox"] = tkinter.Listbox(
        station_data["frame"],
        highlightthickness=0,
        borderwidth=0,
        activestyle="none",
        selectmode="extended",
        height=10,  # um yea scrolling
    )
    for commodity_name, commodity in body["goods"].items():
        station_data["listbox"].insert(
            "end", f"{commodity['price']}SHM {commodity_name}"
        )
        station_data["goods_by_id"].append(commodity)
    station_data["listbox"].pack()
    for id, commodity in enumerate(station_data["goods_by_id"]):
        commodity["id"] = id
        print("appended id:", commodity)
    print(player["inventory"])

    # v sacred texts go here
    bal_text = tkinter.Label(window, text=f"Balance: {player['bal']}SHM")
    bal_text.grid(row=2, column=0)

    # last but not least, the place to enter The Amount *dramatic music*
    amount_entry = tkinter.Entry(window, width=4, justify="right")
    amount_entry.grid(row=2, column=1)
    amount_entry.insert(0, "10")  # < Default value

    def _trade_update(commodity, amount, listbox, commodity_id):
        """Trade and then update window."""  # < Aaahh english
        if trade(player, commodity, amount):
            name = commodity["name"]

            if "id" in player["inventory"][name]:
                ship_data["listbox"].delete(player["inventory"][name]["id"])
                ship_data["listbox"].insert(
                    player["inventory"][name]["id"],
                    f"{player['inventory'][name]['amount']}t {name}",
                )
            else:
                ship_data["listbox"].insert(
                    "end", f"{player['inventory'][name]['amount']}t {name}"
                )
                ship_data["goods_by_id"].append(commodity)
                player["inventory"][name]["id"] = (
                    len(ship_data["goods_by_id"]) - 1
                )
            listbox.select_set(commodity_id)
            listbox.event_generate("<<ListboxSelect>>")
            bal_text.config(text=f"Balance: {player['bal']}SHM")

    return window


if __name__ == "__main__":
    import data

    init_window(
        {
            "crew": 7,
            "inventory": {
                "food": comm_constr("food", 10),
                "uranium": comm_constr("uranium", 34),
            },  # handle undefined
            "goal": "Mars",
            "ship_id": 0,
            "bal": 100000,
            "location": "Mars",
        },
        data.bodies["Earth"],
    ).mainloop()
